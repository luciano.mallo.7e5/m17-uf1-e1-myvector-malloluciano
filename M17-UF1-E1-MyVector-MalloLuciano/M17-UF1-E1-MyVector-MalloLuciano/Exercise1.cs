﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_MalloLuciano
{
    class Exercise1
    {
        internal void Exercise() {

            ShowMessage(SaveString("Write your name: "));

        }


        internal string SaveString(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }


        internal void ShowMessage(string name){

            Console.Clear();
            Console.WriteLine("Hello " + name);
            Console.ReadKey();

            }

    }
}
