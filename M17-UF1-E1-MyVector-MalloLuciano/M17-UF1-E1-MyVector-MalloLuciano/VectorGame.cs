﻿using System;
using System.Collections.Generic;
using System.Text;
using static M17UF1E1MyVecto.MyVector;

namespace M17UF1E1MyVecto
{
    class VectorGame
    {


        public MyVector[] RandomVectors(int length)
        {

            MyVector[] vectors = new MyVector[length];


            for (int i = 0; i < length; i++)
            {

                int[] coordinates = new int[4];

                Random randNum = new Random();

                for (int j = 0; j < 4; j++)
                {

                    coordinates[j] = randNum.Next(-20, 20);

                }
                MyVector vector = new MyVector(coordinates);
                vectors[i] = vector;
            }


            return vectors;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vectors"></param>
        /// <param name="sort">bool value if it's true sort the array by distance, else by close to the origin. </param>
        public void SortVectors(MyVector[] vectors, bool sort)
        {

            if (sort)
            {
                for (int j = 0; j < vectors.Length - 1; j++)
                {
                    for (int i = 0; i < vectors.Length - 1; i++)
                    {


                        if (vectors[i].CalculateDintance() > vectors[i + 1].CalculateDintance())
                        {

                            MyVector aux = vectors[i];
                            vectors[i] = vectors[i + 1];
                            vectors[i + 1] = aux;
                        }

                    }
                }

            }
            else {
                for (int j = 0; j < vectors.Length - 1; j++)
                {
                    for (int i = 0; i < vectors.Length - 1; i++)
                    {


                        if (vectors[i].CalculateDistanceFromOrigin() > vectors[i + 1].CalculateDistanceFromOrigin())
                        {

                            MyVector aux = vectors[i];
                            vectors[i] = vectors[i + 1];
                            vectors[i + 1] = aux;
                        }

                    }
                }

            }


        }


        public int CalculateDistanceFromOrigin(MyVector vector) {

        int distance = Convert.ToInt32((((vector._coordinates[2] - (vector._coordinates[0])) * (vector._coordinates[1])) - ((vector._coordinates[0]) * (vector._coordinates[3] - vector._coordinates[1]))) / vector.CalculateDintance());
        return distance;
        }
    }
}

/*Implementa els següents requisits:
(Bonus) Crea una funció que representa visualment la direcció d’un vector (utilitzant caràcters a la consola)*/

