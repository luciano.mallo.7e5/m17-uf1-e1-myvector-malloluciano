﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17UF1E1MyVecto
{
    class MyVector
    {
        public int[] _coordinates = new int[4];

        public MyVector(int x0, int y0, int x1, int y1)
        {

            _coordinates = new int[] {  x0, y0 , x1, y0  };

        }
        public MyVector(int[] coodinates)
        {

            _coordinates = coodinates;

        }


        public int[] GetCoordenates()
        {
            return _coordinates;

        }

        public void SetCoordenates(int[] coordinates) {
           
               
                      _coordinates = coordinates; 

                    
        }

        public int CalculateDintance()
        {

           return Math.Abs(Convert.ToInt32(Math.Sqrt(Math.Pow(_coordinates[0]- _coordinates[2], 2.0)+ Math.Pow(_coordinates[1] - _coordinates[3], 2.0))));

            
        }

        public void ChangeDirection() {

           var temp = (_coordinates[0], _coordinates[1]);
           (_coordinates[0], _coordinates[1]) = (_coordinates[2], _coordinates[3]);
            (_coordinates[2], _coordinates[3]) = temp;
        }



        public override string ToString() {


            return "\nVector's stats: \nCoordinates: "+"\n Starts at [" + _coordinates[0]+","+_coordinates[1]+"]"+ "\n Finish at [" +_coordinates[2] +","+ _coordinates[3]+"]"+"\n Distance: " + CalculateDintance()+"\n Distance to the origin: " + CalculateDistanceFromOrigin();
        }

        public int CalculateDistanceFromOrigin()
        {

            int distance = Math.Abs(Convert.ToInt32((((_coordinates[2] - (_coordinates[0])) * (_coordinates[1])) - ((_coordinates[0]) * (_coordinates[3] - _coordinates[1]))) / CalculateDintance()));
            return distance;
        }


    }
}
