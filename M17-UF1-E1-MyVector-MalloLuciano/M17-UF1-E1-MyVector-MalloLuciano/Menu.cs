﻿using M17UF1E1MyVecto;
using static M17UF1E1MyVecto.VectorGame;
using System;


namespace M17_UF1_E1_MyVector_MalloLuciano
{
    public class Menu
    {
        static Exercise1 exercise = new Exercise1();
        static VectorGame exercise2 = new VectorGame();

        static void Main()
        {
            bool fi;
            do
            {
                ShowMenu();
                fi = TratarOpcion();
            } while (!fi);
        }
        public static bool TratarOpcion()

        {
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "0":

                    return true;
                case "1":
                    Console.Clear();
                    Exercise1();
                    break;

                case "2":
                    Console.Clear();
                    Exercise2();
                    break;

                case "3":
                    Console.Clear();
                    Exercise3();
                    break;

                case "4":
                    Console.Clear();
                    Bonus();
                    break;
                default:

                    Console.WriteLine("Wrong option!\n");
                    break;
            }

            return false;
        }

        public static void ShowMenu()
        {
            Console.WriteLine("\n Press the number of exercise to run plus enter");

            Console.WriteLine("1- Introducció: Hello {name}");
            Console.WriteLine("2 - Classe MyVector");
            Console.WriteLine("3 - VectorGame");
            Console.WriteLine("0 - Exit.");

        }

        public static void Exercise1() {
            
            Console.WriteLine("\t\tExercise 1");
            Console.WriteLine("\n");
            exercise.Exercise();
            Console.Clear();
        }

        public static void Exercise2(){

            Console.WriteLine("\t\tExercise 2");
            Console.WriteLine("\n");


            MyVector vector = new MyVector(2, 1, 5, 6);

            Console.WriteLine(vector);
            Console.WriteLine();
            Console.WriteLine("Old Coordinates [" + string.Join("," , vector.GetCoordenates()) + "]");
            Console.WriteLine();
            vector.SetCoordenates(new int[] { 5,8,9,10 });
            Console.WriteLine();
            Console.WriteLine("New Coordinates [" + string.Join(",", vector.GetCoordenates())+"]");
            Console.WriteLine();
            Console.WriteLine("The distance of the vector is "+vector.CalculateDintance());
            Console.WriteLine("\nVector with new coordinates");
            Console.WriteLine(vector);
            vector.ChangeDirection();
            Console.WriteLine("\nVector with new direction");
            Console.WriteLine(vector);
        }

        public static void Exercise3() {

            Console.WriteLine("\t\tExercise 3");
            Console.WriteLine();

            MyVector[] vectors = exercise2.RandomVectors(4);

            Console.WriteLine("All vectors filled randomly");

            foreach (MyVector vector in vectors) {
                Console.WriteLine(vector);
                Console.WriteLine("\n");
            }

            Console.WriteLine("All vectors sorted by distance");

            exercise2.SortVectors(vectors,true);

            foreach (MyVector vector in vectors)
            {
                Console.WriteLine(vector);
                Console.WriteLine("\n");
            }

            Console.WriteLine("All vectors sorted by distance to the origin");

            exercise2.SortVectors(vectors, false);

            foreach (MyVector vector in vectors)
            {
                Console.WriteLine(vector);
                Console.WriteLine("\n");
            }


        }

        public static void Bonus() { 
        
        
        
        
        }
    }
}
